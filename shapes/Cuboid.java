import java.util.List;
import java.util.Arrays;
// TODO : a missing interface method must be implemented in this class to make it compile. This must be in terms of volume().

public class Cuboid implements ThreeDShape, SurfaceArea {

    private final ThreeDPoint[] vertices = new ThreeDPoint[8];

    /**
     * Creates a cuboid out of the list of vertices. It is expected that the vertices are provided in
     * the order as shown in the figure given in the homework document (from v0 to v7).
     * 
     * @param vertices the specified list of vertices in three-dimensional space.
     */
    public Cuboid(List<ThreeDPoint> vertices) {
        if (vertices.size() != 8)
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                                                             this.getClass().getName()));
        int n = 0;
        for (ThreeDPoint p : vertices) this.vertices[n++] = p;
    }

    public double getSA(){
        double height = vertices[0].distTo(vertices[3]);
        double depth = vertices[0].distTo(vertices[5]);
        double width = vertices[0].distTo(vertices[2]);

        return 2 * (height*width + height*depth + depth*width);
    }

    @Override
    public double volume() {
        //note formula needs to account for object rotated in space
	    double height = vertices[0].distTo(vertices[3]);
        double depth = vertices[0].distTo(vertices[5]);
        double width = vertices[0].distTo(vertices[2]);
	    return height*depth*width; 
    }

    @Override
    public ThreeDPoint center() {
        return vertices[0].midpoint(vertices[7]);
    }
    
    @Override
    public int compareTo(ThreeDShape other){
        return (int)Math.signum(this.volume() - other.volume()); //Math.signum used to avoid cast errors
                                                                 //e.g. (int)0.5 -> 0
                                                                 //ugly but maybe necessary
    }

    public static Cuboid random(){
        double x0 = 0;
        double y0 = 0;
        double z0 = 0;
        double len1 = Math.random()*5;
        double len2 = Math.random()*5;
        double len3 = Math.random()*5;

        ThreeDPoint pt0 = new ThreeDPoint(x0,y0,z0);
        ThreeDPoint pt1 = new ThreeDPoint(x0-len1,y0,z0);
        ThreeDPoint pt2 = new ThreeDPoint(x0-len1,y0-len2,z0);
        ThreeDPoint pt3 = new ThreeDPoint(x0,y0-len2,z0);
        ThreeDPoint pt4 = new ThreeDPoint(x0,y0-len2,z0-len3);
        ThreeDPoint pt5 = new ThreeDPoint(x0,y0,z0-len3);
        ThreeDPoint pt6 = new ThreeDPoint(x0-len1,y0,z0-len3);
        ThreeDPoint pt7 = new ThreeDPoint(x0-len1,y0-len2,z0-len3);

        ThreeDPoint[] pts = {pt0,pt1,pt2,pt3,pt4,pt5,pt6,pt7};
        return new Cuboid(Arrays.asList(pts)); 
    }    
}
