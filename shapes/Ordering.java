import java.util.*;

public class Ordering {

    static class XLocationComparator implements Comparator<TwoDShape> {
        @Override public int compare(TwoDShape o1, TwoDShape o2) {
            if ((!(o1 instanceof SmallestX)) || (!(o2 instanceof SmallestX)))
                throw new IllegalArgumentException();            
            double result = ((SmallestX)o1).smallestX() - ((SmallestX)o2).smallestX();
            return (int)Math.signum(result);
        }
    }

    static class AreaComparator implements Comparator<SymmetricTwoDShape> {
        @Override 
        public int compare(SymmetricTwoDShape o1, SymmetricTwoDShape o2) {
            double result = o1.area() - o2.area();
            return (int)Math.signum(result);
        }
    }

    static class SurfaceAreaComparator implements Comparator<ThreeDShape> {
        @Override 
        public int compare(ThreeDShape o1, ThreeDShape o2) {
            if ((!(o1 instanceof SurfaceArea)) || (!(o2 instanceof SurfaceArea)))
                throw new IllegalArgumentException();            
            double result = ((SurfaceArea)o1).getSA() - ((SurfaceArea)o2).getSA();
            return (int)Math.signum(result); 
        }
    }

   
    static <A> void copy(Collection<? extends A> source, Collection<A> destination) {
        destination.addAll(source);
    }

    public static void main(String[] args) {
        List<TwoDShape>          shapes          = new ArrayList<>();
        List<SymmetricTwoDShape> symmetricshapes = new ArrayList<>();
        //List<ThreeDShape>        threedshapes    = new ArrayList<>();

        /*
         * uncomment the following block and fill in the "..." constructors to create actual instances. If your
         * implementations are correct, then the code should compile and yield the expected results of the various
         * shapes being ordered by their smallest x-coordinate, area, volume, surface area, etc. */

        double[] r1 = {0,0,0,4,3,4,3,0};
        double[] s1 = {0,0,0,2,2,2,2,0};
        Rectangle test = new Rectangle(TwoDPoint.ofDoubles(r1));

        symmetricshapes.add(new Rectangle(TwoDPoint.ofDoubles(r1)));
        symmetricshapes.add(new Square(TwoDPoint.ofDoubles(s1)));
        symmetricshapes.add(new Circle(-2,3,4));

        copy(symmetricshapes, shapes); // note-1 //
        double[] q1 = {1d,2d,3d,4d,5d,6d,7d,8d};
        shapes.add(new Quadrilateral(TwoDPoint.ofDoubles(q1)));//TODO:
        

        // sorting 2d shapes according to various criteria
        shapes.sort(new XLocationComparator());
        symmetricshapes.sort(new XLocationComparator());
        symmetricshapes.sort(new AreaComparator());

        // sorting 3d shapes according to various criteria
        //Collections.sort(threedshapes);
        //threedshapes.sort(new SurfaceAreaComparator());

        /*
         * if your changes to copy() are correct, uncommenting the following block will also work as expected note that
         * copy() should work for the line commented with 'note-1' while at the same time also working with the lines
         * commented with 'note-2' and 'note-3'. */

        
        List<Number> numbers = new ArrayList<>();
        List<Double> doubles = new ArrayList<>();
        Set<Square>        squares = new HashSet<>();
        Set<Quadrilateral> quads   = new LinkedHashSet<>();

        copy(doubles, numbers); // note-2 //
        copy(squares, quads);   // note-3 //
        
    }
}
