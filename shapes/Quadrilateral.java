import java.util.Arrays;
import java.util.List;

public class Quadrilateral implements Positionable, TwoDShape, SmallestX {

    protected final TwoDPoint[] vertices = new TwoDPoint[4];
    
    @Override
    public double smallestX(){
        double min = vertices[0].coordinates()[0];

        for (int i = 1; i<4; i++){
            if (vertices[i].coordinates()[0] < min)
                min = vertices[i].coordinates()[0];
        }
        return min;
    }
    
    public Quadrilateral(double[] vertices) { 
        this(TwoDPoint.ofDoubles(vertices));
    }

    public Quadrilateral(List<TwoDPoint> vertices) {//no nullptr in this function
        int n = 0;
        for (TwoDPoint p : vertices) this.vertices[n++] = p;
        if (!isMember(vertices))
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                                                             this.getClass().getCanonicalName()));
    }

    /**
     * Given a list of four points, adds them as the four vertices of this quadrilateral in the order provided in the
     * list. This is expected to be a counterclockwise order of the four corners.
     *
     * @param points the specified list of points.
     * @throws IllegalStateException if the number of vertices provided as input is not equal to four.
     */
    @Override
    public void setPosition(List<? extends Point> points) {
        
	    if (points.size() != 4)
		    throw new IllegalStateException(String.format("Invalid set of vertices specified for %s",
                                                             this.getClass().getName()));	
		
        int n = 0;
        for (Point p : points) this.vertices[n++] = (TwoDPoint)p;
    }

    @Override
    public List<TwoDPoint> getPosition() {
        return Arrays.asList(vertices);
    }

    /**
     * @return the lengths of the four sides of the quadrilateral. Since the setter {@link Quadrilateral#setPosition(List)}
     *         expected the corners to be provided in a counterclockwise order, the side lengths are expected to be in
     *         that same order.
     */
    protected double[] getSideLengths() {
        double[] result = new double[4];
        for (int i=0; i<4; i++){
            result[i] = vertices[i].distTo( vertices[(i+1)%4] );        
        }        
        return result; 
    }

    @Override
    public int numSides() { return 4; }

    @Override
    public boolean isMember(List<? extends Point> vertices) { return vertices.size() == 4; }
}
