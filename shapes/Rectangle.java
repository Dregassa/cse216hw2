import java.util.List;

public class Rectangle extends Quadrilateral implements SymmetricTwoDShape {

    public Rectangle(List<TwoDPoint> vertices) {
        super(vertices);        
    }

    /**
     * The center of a rectangle is calculated to be the point of intersection of its diagonals.
     *
     * @return the center of this rectangle.
     */
    @Override
    public Point center() {
        
        return vertices[0].midpoint(vertices[2]);
    }

    @Override
    public boolean isMember(List<? extends Point> vertices) {
        //use the fact that the diagonals of a rectangle bisect each other, so their midpoints are equal
        //TODO: should do something about unsafe casting, like throw some IllegalArgExcptn or false bool
        if (vertices.size()!=4) return false;
        //if (!(? extends TwoDPoint)) return false;
        TwoDPoint midpt0 = ((TwoDPoint)vertices.get(0)).midpoint((TwoDPoint)vertices.get(2));
        TwoDPoint midpt1 = ((TwoDPoint)vertices.get(1)).midpoint((TwoDPoint)vertices.get(3));
        return midpt0.equals(midpt1); 
    }

    @Override
    public double area() {
        return (vertices[0].distTo(vertices[1])) * (vertices[0].distTo(vertices[3])); 
    }
}
