import java.util.Collections;
import java.util.List;

public class Sphere implements Positionable, ThreeDShape, SurfaceArea{

    private ThreeDPoint center;
    private double radius;

    public Sphere(double centerx, double centery, double centerz, double radius) {
        this.center = new ThreeDPoint(centerx, centery, centerz);
        this.radius = radius;
    }

    @Override
    public double getSA(){
        return Math.PI * 4 * Math.pow(radius,2);
    }

    @Override
    public void setPosition(List<? extends Point> points){
        if (!(points.get(0) instanceof ThreeDPoint))
		    throw new IllegalArgumentException();
	    center = (ThreeDPoint)points.get(0);
    }

    @Override
    public List<? extends Point> getPosition(){
        return Collections.singletonList(center);
    }

    @Override    
    public Point center(){
        return center;
    }

    @Override
    public double volume(){
        return Math.PI * 4 * Math.pow(radius,3) / 3;
    }
     
    public void setRadius(double r) { this.radius = r; }

    public double getRadius()       { return radius; }

    @Override
    public int compareTo(ThreeDShape other){
        return (int)Math.signum(this.volume() - other.volume());
    }

    public static Sphere random(){
        double newRadius = Math.random() * 5;
        return new Sphere(0,0,0,newRadius);
    }    
}
