/**
 * An unmodifiable point in the three-dimensional space. The coordinates are specified by exactly three doubles (its
 * <code>x</code>, <code>y</code>, and <code>z</code> values).
 */

import java.lang.Math;

public class ThreeDPoint implements Point {
	
    final double x;
    final double y;
    final double z;    

    public ThreeDPoint(double x, double y, double z) {
        this.x = x;
	    this.y = y;
        this.z = z;
    }

    /**
     * @return the (x,y,z) coordinates of this point as a <code>double[]</code>.
     */
    @Override
    public double[] coordinates() {
	    double[] result = new double[3];
    	result[0] = x;
    	result[1] = y;
    	result[2] = z;        
    	return result; 
    }

    public double distTo(ThreeDPoint other){
        double xdist = this.x - other.x;
        xdist *= xdist; //square
        double ydist = this.y - other.y;
        ydist *= ydist;
        double zdist = this.z - other.z;
        zdist *= zdist;
        return Math.sqrt(xdist + ydist + zdist);        
    }
    
    public ThreeDPoint midpoint (ThreeDPoint other){
        return new ThreeDPoint(   (this.x+other.x)/2,
                                  (this.y+other.y)/2,
                                  (this.z+other.z)/2
                              );                                                
    }
}
