import java.util.List;
import java.lang.Math;
import java.util.ArrayList;
/**
 * An unmodifiable point in the standard two-dimensional Euclidean space. The coordinates of such a point is given by
 * exactly two doubles specifying its <code>x</code> and <code>y</code> values.
 */
public class TwoDPoint implements Point {
	
    private final double x;
    private final double y;
    private static final double EQUALITY_THRESHOLD = .00001;
    
    

    public TwoDPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return the coordinates of this point as a <code>double[]</code>.
     */
    @Override
    public double[] coordinates() {
        double[] result = new double[2];
        result[0] = x;
        result[1] = y;        
        return result; 
    }

    public double distTo(TwoDPoint other){
        double xdist = x - other.x;
        xdist *= xdist; //square
        double ydist = y - other.y;
        ydist *= ydist;
        return Math.sqrt(xdist + ydist);        
    }
    
    public TwoDPoint midpoint (TwoDPoint other){
        TwoDPoint result = new TwoDPoint((this.x+other.x)/2,(this.y+other.y)/2);
        return result;                                              
    }
    /**
     * Returns a list of <code>TwoDPoint</code>s based on the specified array of doubles. A valid argument must always
     * be an even number of doubles so that every pair can be used to form a single <code>TwoDPoint</code> to be added
     * to the returned list of points.
     *
     * @param coordinates the specified array of doubles.
     * @return a list of two-dimensional point objects.
     * @throws IllegalArgumentException if the input array has an odd number of doubles.
     */
    public static List<TwoDPoint> ofDoubles(double[] coordinates) throws IllegalArgumentException {
	    if (coordinates.length % 2 == 1) 
	        throw new IllegalArgumentException("Even number of vertices required");
        List<TwoDPoint> result = new ArrayList<TwoDPoint>();
        for(int i = 0; i < coordinates.length/2; i++){
            double x = coordinates[2*i];
            double y = coordinates[2*i + 1];
            TwoDPoint tmp = new TwoDPoint(x,y);        
            result.add(tmp);
        }

	    return result; 
    }

    public boolean equals (TwoDPoint other){
        return Math.abs(this.x-other.x) < EQUALITY_THRESHOLD &&
               Math.abs(this.y-other.y) < EQUALITY_THRESHOLD;
    }   

    public TwoDPoint round(){
        return new TwoDPoint(Math.rint(x), Math.rint(y));
    }
}
